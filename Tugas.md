Tugas Portofolio Ujian Tengah Semester Analisis dan Perancangan Perangkat Lunak
Aplikasi Penyewaan Mobil Berbasis Aplikasi

 
Arkan Fathoni
2000018057
A
https://gitlab.com/arkan2000018057
Capture use case global :
 
•	Ruang lingkup/deskripsi dari sistem
Menggunakan aplikasi memudahkan peminjam dan pemilik mobil dalam melakukan transaksi, penyewa tidak perlu datang terlebih dahulu ketempat, slot yang tidak tersedia atau sudah di sewa tidak akan ditampiklan sehingga lebih cepat dalam transaksi, dan data secara otomatis disimpan
-	Penyewa dapat melakukan login, peminjaman, pembayaran, pembatalan
-	Pemilik dapat menambah, mengubah, menghapus data, serta login sebagai pemilik
-	User bisa melihat data transaksi yang sudah dilakukan


•	Daftar seluruh spesifikasi kebutuhan (SRS) sistem
fungsional :
-Sistem harus bisa menampilkan produk yang disewakan, beserta waktu dan tanggal yang tersedia/sudah disewakan
-Sistem dapat mencatat transaksi apa saja yang sudah terjadi
-Pengguna dapat melihat data produk yang disediakan beserta data waktu dan tanggal penyewaan produk
-Pengguna bisa melakukan pembayaran dengan aplikasi
-Pengguna bisa melakukan penyewaan dan pembatalan penyewaan
-Pengguna bisa melakukan pengelolaan data pada akum pemilik

non fungsional :
-Sistem meminta login kepada pengguna
-Sistem berjalan tanpa error atau lag
-Tidak adanya kebocoran data pada pihak ketiga
-Ada backup data jika terjadi masalah









•	Proses pembuatan Use case, disertai penjelasan proses dan bagian-bagiannya
 
 		Aktor : Sebagai pelaku yang melakukan suatu tindakan
 Use case : Fungsional sebuah sistem
 Subject : MEnentukan jenis sistem dan ruang lingkup/batasan
 Asosiasi : Menghubungkan Aktor dan use case
 Relasi Extend : Untuk menunjukkan usecase tambahan yang bersifat opsional



•	Skenario analisis use case, disertai penjelasan proses dan bagian-bagiannya

Login
Aktor	Sistem
Sekenario utama
1.	Memasukkan user name dan password	
	2.	Vertifikasi data
	3.	Masuk ke aplikasi
Skenario Alternatif
1.	Memasukkan user name dan password	
	2.	Vertifikasi data
	3.	Memberikan pesan login tidak valid

Melakukan penyewaan
Aktor	Sistem
Skenario utama
	1.	Sistem menampilkan berbagai jenis mobil
2.	User mencari pilihannya 	
3.	User memlih yang paling sesuai	
	4.	Sistem memproses ke tampilan pembayaran

Pembayaran
Aktor	Sistem
Skenario utama
	1.	Sistem menampilkan tampilan pembayaran
2.	User memilih metode pembayaran	
3.	User memasukkan pin pembayaran	
	4.	Vertifikasi data
	5.	Menampiklan pesan pembayaran sukses
Skenario Alternatif
	1.	Sistem menampilkan tampilan pembayaran
2.	User memilih metode pembayaran	
3.	User memasukkan pin pembayaran	
	4.	Vertifikasi data
	5.	Menampilkan pesan pembayaran gagal



Pembatalan penyewaan
Aktor	Sistem
Skenario utama
	1.	Sistem menampilkan tampilan pembatalan
2.	User melakukan pembatalan	
	3.	Sistem memproses pembatalan
	4.	Menampilkan pesan pembatalan

Pengelolaan data
Aktor	Sistem
Skenario utama
	1.	Sistem menampilkan pengelolaan data
2.	User memilih salah satu pilihan	
3.	User melakukan pengelolaan data	
	4.	Sistem menyimpan perubahan

Melihat data transaksi
Aktor	Sistem
Akenario utama
	1.	Sistem menampilkan data transaksi
2.	User melihat lihat data transaksi	










•	Gambar diagram activity beserta penjelasan tiap diagram, yang merupakan penjabaran dari semua proses di use case

1.	Login
 
2.	Melakukan penyewaan
 







3.	Pembayaran
 
4.	Pembatalan penyewaan
 







5.	Pengeditan produk
 
6.	Melihat data transaksi
 

•	Screenshot daftar file di folder projek pada Gitlab (full tampilan browser) dan diberi penjelasan secukupnya

•	Analisis pengerjaan projek (tinjauan dari sisi waktu, ketercapaian spesifikasi, biaya yang dibutuhkan, kendala, tantangan masa depan, dan atau lain-lain jika ada)
Waktu			: Setiap proses dilakukan tiap minggu
Spesifikasi		: Spesifikasi tercapai
Biaya			: Selalu mengguankan aplikasi yang gratis, untuk sekarang Rp.0
Kendala			: Menghubungkan setiap proses menjadi satu kesatuan
Tantangan masa depan	: Proses pembuatan aplikasinya
